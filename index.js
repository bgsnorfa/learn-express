const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");

const app = express();
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
// app.set("view engine", "ejs");
app.use(express.static(__dirname + '/public'));

const port = "3000";
const authToken = "abcdef";

const jsonParser = bodyParser.json();

let phoneNumbers = require("./registeredPhoneNumber");

// Middlewares
const logger = (req, res, next) => {
  console.log(`Requesting: ${req.method} - ${req.url}`);
  next();
}

const verifyAuthToken = (req, res, next) => {
  let auth = req.headers['authorization'];
  if (auth == authToken) {
    next();
  } else {
    res.status(401).json({
      "message": "unauthorized"
    });
  }
}

const checkLocation = (req, res, next) => {
  let location = req.headers['location'];
  if (location !== "Indonesia") {
    res.status(403).json({
      "message": "Forbidden, cannot access from your location"
    });
  } else {
    next();
  }
}

// Routes

app.get("/", logger, (req, res) => {
  res.render('index', {
    "name": "Jhon LBD"
  });
});

app.get("/product", logger, (req, res) => {
  res.json([
    "Apple",
    "Redmi",
    "One Plus One"
  ]);
});

app.get("/order", logger, (req, res) => {
  res.json([
    {id: 1, paid: false, user_id: 1},
    {id: 2, paid: false, user_id: 1}
  ]);
});

app.get("/list-phone-numbers", logger, (req, res) => {
  res.json(phoneNumbers);
});

app.post("/search-phone-number", logger, jsonParser, (req, res) => {
  let body = req.body;

  let isFound = phoneNumbers.indexOf(body.query) !== -1;
  // -1 !== -1 => false
  // 0 !== -1 => true
  // 5 !== -1 => true

  res.json({
    data_found: isFound
  });
});

let queue = [];

app.get("/queue", jsonParser, verifyAuthToken, checkLocation, (req, res) => {
  res.render('queue', {
    queue
  });
});

app.post("/queue", jsonParser, verifyAuthToken, checkLocation, (req, res) => {
  let body = req.body;
  let name = body.name;
  queue.push(name);

  res.send({
    message: "Queue updated"
  });
});

app.delete("/queue", jsonParser, verifyAuthToken, checkLocation, (req, res) => {
  let user = queue[0];
  queue.shift();

  let message = "No queue";

  if (user !== undefined) {
    message = `${user} finished`;
  }

  res.send({
    message: message
  });
});

app.get("/page.html", (req, res) => {
  res.sendFile(path.join(__dirname, '/page.html'));
});

app.get("/style.css", (req, res) => {
  res.sendFile(path.join(__dirname, '/style.css'));
});

// Submission from group 1
app.post('/calculator', jsonParser, (req,res) => {
  let body = req.body
  let num1 = body.num1
  let num2 = body.num2
  let operator = body.operator
  let result = 0

  switch(operator) {
      case '+':
          result = num1 + num2
          break
      case '-':
          result = num1 - num2
          break
      case '*':
          result = num1 * num2
          break
      case '/':
          result = num1 / num2
          break
      default:
          result = "Error"
  }
  res.send({
    result: result
  })
})

// Submission from group 2
app.post('/rps', jsonParser, (req, res) => {
  let body = req.body;
  let result = ''
  let computerChoices = ['rock', 'paper', 'scissors'];
  let computer = Math.floor(Math.random() * computerChoices.length)
  computer = computerChoices[computer]
  switch (req.body.input) {
    case "rock":
      if (computer === "scissors") {
        result = 'User win'
      } else if (computer === "paper") {
        result = "Computer Win"
      } else {
        result = "Draw"
      }
      break;
    case "paper":
      if (computer === "rock") {
        result = 'User win'
      } else if (computer === "scissors") {
        result = "Computer win"
      } else {
        result = "Draw"
      }
      break;
      case "scissors":
        if (computer === "paper") {
          result = 'User win'
        } else if (computer === "rock") {
          result = "Computer win"
        } else {
          result = "Draw"
        }
        break;
  }

  res.json({
      "com_choice": computer,
      "winner": result,
  })
})

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});